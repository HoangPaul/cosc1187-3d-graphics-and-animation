# 3D Graphics and Animation, Semester 1 2014 #

3D Graphics and Animation is an introductory course to OpenGL offered at RMIT. This repository contains code that was submitted as part of the first and second assignment.

### At a glance ###
Written in C using OpenGL.  
Assignment 1: Procedurally generated mesh with movable player.  
Assignment 2: Extends the first assignment with simple physics, collision detection, split screen, win conditions, infinite terrain, skybox, and other things that I may have forgotten.  
** Note: ** Assignment 2 is built on skeleton code given by the lecturer/tutor.

### Contributions ###
**Paul Hoang**  
PaulKhoaHoang {at} gmail.com   

**Geoff Leach/Jesse Archer** (Lecturer/head tutor)  
gl {at} rmit.edu.au  
jesse.archer {at} rmit.edu.au