#define _USE_MATH_DEFINES 
#include <math.h>
typedef struct {
    float x, y, z;
} vertex_t;
void normalise(float x, float y, float z, float *nx, float *ny, float *nz); 
float angle_between_vec(float x1, float y1, float z1, float x2, float y2, float z2);
