/*Paul Hoang s3383494*/
#if _WIN32
#   include <Windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "Helper.h"

#define GRID_WIDTH 40
#define GRID_HEIGHT 80
#define GRID_SHIRNK 10.0
/*Keyboard*/
#define WIREFRAME   'p' - 97
#define AXES        'o' - 97
#define NORMAL      'n' - 97
#define BINORMAL    'b' - 97
#define TANGENT     't' - 97
#define LIGHTING    'l' - 97
#define FORWARD     'w' - 97
#define BACKWARD    's' - 97
#define LEFT        'a' - 97
#define RIGHT       'd' - 97
#define QUIT        'q' - 97

#define FUNCTION_X(x,z) 0.1 * sin(x  * M_PI * 2 + z)
#define FUNCTION_Z(x,z) 0.3 * sin(z  * M_PI + x) - z * 0.3
#define DERIVATIVE_X(x,z) 2 * M_PI * 0.1 * cos(x * M_PI * 2 + z) 
#define DERIVATIVE_Z(x,z) M_PI * 0.2 * cos(z * M_PI + x) - 0.3

#define PLAYER_SIZE 0.05

vertex_t grid   [GRID_WIDTH * GRID_HEIGHT * 2];
vertex_t normals[GRID_WIDTH * GRID_HEIGHT * 2];

/*Mouse rotation*/
int angle_x = 0;
int angle_y = 0;
int mouse_start_x = 0;
int mouse_start_y = 0;
int change_x = 0;
int change_y = 0;
int isLeftClick = 0;
/*Mouse translation*/
int mouse_start_right_z = 0;
int change_right_z = 0;
int isRightClick = 0;
/*keyboard*/
int keyboard[26];
/*Player*/
float pos_x = 0;
float pos_y = 0;
float pos_z = 0;
float rot_y = 1;
/*Camera*/
float cam_x = 0;
float cam_z = 0;
float zoom = 0;

void construct_grid() {
    int i, j, count = 0;

    while (count < GRID_WIDTH * GRID_HEIGHT * 2) {
        grid[count].x = 0;
        grid[count].y = 0;
        grid[count].z = 0;
        count++;
    }

    count = 0;
    for (j = 0; j < GRID_HEIGHT; j++) {
        for (i = 0; i < GRID_WIDTH; i++) {
            if (grid[count].x != 0 ||
                    grid[count].y != 0 ||
                    grid[count].z != 0) {
                printf("not zero\n");
            }
            grid[count].x = (float)-GRID_WIDTH/GRID_SHIRNK/2 + (float)i/GRID_SHIRNK;
            grid[count].z = (float)-GRID_HEIGHT/GRID_SHIRNK/2 + (float)j/GRID_SHIRNK + 1.0/GRID_SHIRNK;
            grid[count].y = FUNCTION_X(grid[count].x, grid[count].z) + FUNCTION_Z(grid[count].x, grid[count].z);
            count++;
            grid[count].x = (float)-GRID_WIDTH/GRID_SHIRNK/2 + (float)i/GRID_SHIRNK;
            grid[count].z = (float)-GRID_HEIGHT/GRID_SHIRNK/2 + (float)j/GRID_SHIRNK;
            grid[count].y = FUNCTION_X(grid[count].x, grid[count].z) + FUNCTION_Z(grid[count].x, grid[count].z);
            count++;
        }
    }
}

void init() {
    int i;
    construct_grid();
    for (i = 0; i < 26; i++) {
        keyboard[i] = 0;
    }
}

void draw_grid() {
    int i, j, count = 0;
    /*grey snow*/
    float params1[] = {0.75, 0.75, 0.75, 1};

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, params1);

    /*white colour for wireframe*/
    glColor3f(1, 1, 1);
    for (j = 0; j < GRID_HEIGHT; j++) {
        glBegin(GL_TRIANGLE_STRIP);
        for (i = 0; i < GRID_WIDTH; i++) {
            glNormal3f(normals[count].x, normals[count].y, normals[count].z);
            glVertex3f(grid[count].x, grid[count].y, grid[count].z);
            count++;
            glNormal3f(normals[count].x, normals[count].y, normals[count].z);
            glVertex3f(grid[count].x, grid[count].y, grid[count].z);
            count++;
        }
        glEnd();
    }
}

void draw_normals() {
    int i, j, count = 0;
    float v1, v2, v3;

    if (keyboard[LIGHTING]) {
        glDisable(GL_LIGHTING);
    }

    glBegin(GL_LINES);
    for (i = 0; i < GRID_HEIGHT; i++) {
        for (j = 0; j < GRID_WIDTH * 2; j++) {
            /*derive*/
            v1 = DERIVATIVE_X(grid[count].x, grid[count].z);
            v2 = 1;
            v3 = DERIVATIVE_Z(grid[count].x, grid[count].z);
            /* normal (cross product)
             *     i   j  k
             * z = 0  v3  v2
             * x = v2 v1  0
             * (-v2*v1)i -(-v2*v2)j + (-v2*v3)k
             */
            /*normalise*/
            normalise(-(v1 * v2), (v2 * v2), -(v2 * v3), &(normals[count].x), &(normals[count].y), &(normals[count].z));

            /*scale down the debug lines*/
            v1 = v1 * 0.1;
            v2 = v2 * 0.1;
            v3 = v3 * 0.1;

            if (keyboard[TANGENT]) {
                /*tangent of x, purple */
                glColor3f(1, 0, 1);
                glVertex3f(grid[count].x, grid[count].y, grid[count].z);
                glVertex3f(grid[count].x + v2, grid[count].y + v1, grid[count].z);
            }
            if (keyboard[BINORMAL]) {
                /* tangent of z, cyan */
                glColor3f(0, 1, 1);
                glVertex3f(grid[count].x, grid[count].y, grid[count].z);
                glVertex3f(grid[count].x, grid[count].y + v3, grid[count].z + v2);
            }
            if (keyboard[NORMAL]) {
                /* normal vector, yellow */
                glColor3f(1, 1, 0); 
                glVertex3f(grid[count].x, grid[count].y, grid[count].z);
                glVertex3f(grid[count].x + normals[count].x * 0.1, grid[count].y + normals[count].y * 0.1, grid[count].z + normals[count].z * 0.1);
            }
            count++;


        }
    }
    glEnd();
    if (keyboard[LIGHTING]) {
        glEnable(GL_LIGHTING);
    }

}

void handle_lighting() {
    float pos[] = {1, 1, 1, 0};
    if (keyboard[LIGHTING]) {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, pos);
    } else {
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
    }
}

void draw_axes() {
    if (!keyboard[AXES]) {
        return;
    }
    if (keyboard[LIGHTING]) {
        glDisable(GL_LIGHTING);
    }
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0,0,0);
    glVertex3f(2,0,0);
    glColor3f(0, 1, 0);
    glVertex3f(0,0,0);
    glVertex3f(0,2,0);
    glColor3f(0, 0, 1);
    glVertex3f(0,0,0);
    glVertex3f(0,0,2);
    glEnd();
    if (keyboard[LIGHTING]) {
        glEnable(GL_LIGHTING);
    }
}

void draw_cube() {
    float corners[8][3] = { {-1, -1,  1}, 
        { 1, -1,  1},
        {-1,  1,  1}, 
        { 1,  1,  1}, 
        { 1,  1, -1}, 
        {-1,  1, -1}, 
        { 1, -1, -1}, 
        {-1, -1, -1}  
    };

    int face[6][4] = {  {0, 1, 3, 2},
        {2, 3, 4, 5},
        {4, 5, 7, 6},
        {7, 6, 1, 0},
        {1, 3, 4, 6},
        {0, 7, 5, 2},
    };
    float normals[6][3] = { { 0, 0, 1},
        { 0, 1, 0},
        { 0, 0,-1},
        { 0,-1, 0},
        { 1, 0, 0},
        {-1, 0, 0}
    };
    /*orange colour*/
    GLfloat params1[] = {1, 0.647, 0, 1};
    int i, j;

    for ( i = 0; i < 8; i++) {
        for (j = 0; j < 3; j++) {
            corners[i][j] = corners[i][j]/2 * PLAYER_SIZE;
        }
    }
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, params1);

    for (i = 0; i < 6; i++) {
        glBegin(GL_POLYGON);

        glNormal3fv(normals[i]);
        glVertex3fv(corners[face[i][0]]);        
        glVertex3fv(corners[face[i][1]]);        
        glVertex3fv(corners[face[i][2]]);        
        glVertex3fv(corners[face[i][3]]);        
        glEnd();
    }
    /*show the face/heading of the player*/
    glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, -0.3);
    glEnd();
}


void draw_player() {
    float x1,y1,z1,xn2,yn2,zn2;
    float angle;

    glPushMatrix();
    
    /*save the pos_y of the player to use for the camera*/
    pos_y = FUNCTION_X(pos_x, pos_z) + FUNCTION_Z(pos_x, pos_z) + PLAYER_SIZE/2;

    glTranslatef(pos_x, pos_y, pos_z);

    /*Find the rotation around the Z axis*/
    x1 = DERIVATIVE_X(pos_x,pos_z);
    y1 = 1;
    z1 = 0;
    /*normal (cross product)
     *     i   j  k
     * z = 0  z1  y1
     * x = y1 x1  0
     * (-y1*x1)i -(-y1*y1)j +(-y1*z1)k
     */
    normalise(-(x1 * y1), (y1 * y1), -(y1 * z1), &xn2, &yn2, &zn2);
    angle = angle_between_vec(xn2, yn2, zn2, 0, 1, 0);


    if (x1 < 0) {
        glRotatef(-angle * 180 / M_PI, 0, 0, 1);
    } else if (x1 >= 0) {
        glRotatef(angle * 180 / M_PI, 0, 0, 1);
    }
    /*Find the rotation around the X axis*/
    x1 = 0;
    y1 = 1;
    z1 = DERIVATIVE_Z(pos_x,pos_z);
    normalise(-(x1 * y1), (y1 * y1), -(y1 * z1), &xn2, &yn2, &zn2);
    angle = angle_between_vec(xn2, yn2, zn2, 0, 1, 0);

    if (z1 < 0) {
        glRotatef(angle * 180 / M_PI, 1, 0, 0);
    } else if (z1 >= 0) {
        glRotatef(-angle * 180 / M_PI, 1, 0, 0);
    }

    /*Roation around the Y axis (player heading)*/
    glRotatef(rot_y * 180 / M_PI, 0, 1, 0);

    draw_cube();
    glPopMatrix();
}

void move_player() {
    if (keyboard[FORWARD]) {
        pos_x += -sin(rot_y) * 0.001;
        pos_z += -cos(rot_y) * 0.001;
        cam_x += sin(rot_y) * 0.001;
        cam_z += cos(rot_y) * 0.001;

    }
    if (keyboard[BACKWARD]) {
        pos_x += sin(rot_y) * 0.001;
        pos_z += cos(rot_y) * 0.001;
        cam_x += -sin(rot_y) * 0.001;
        cam_z += -cos(rot_y) * 0.001;

    }
    if (keyboard[LEFT]) {
        rot_y += 0.01;
    }
    if (keyboard[RIGHT]) {
        rot_y += -0.01;
    }
}

void transformCamera() {
    glTranslatef(0, 0, (zoom  + change_right_z)/100.0f);
    glRotatef(angle_x + change_x, 0, 1, 0);
    glRotatef(angle_y - change_y, 1, 0, 0);
    glTranslatef(cam_x, -pos_y, cam_z);
}

void display() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    if (keyboard[WIREFRAME]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    transformCamera();   

    handle_lighting();
    move_player();
    draw_player();
    draw_normals();
    draw_grid();
    draw_axes();

    glutSwapBuffers();
}


void idle() {
    glutPostRedisplay();
}

void mouseMove(int x, int y) {
    if (isLeftClick) {
        change_x = mouse_start_x - x;
        change_y = mouse_start_y - y;
    } else if (isRightClick) {
        change_right_z = mouse_start_right_z - y;
    }
}

void mouseStart(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        mouse_start_x = x;
        mouse_start_y = y;
        isLeftClick = 1;
    } else if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        angle_x += change_x;
        angle_y -= change_y;
        change_y = 0;
        change_x = 0;
        isLeftClick = 0;
    } else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
        mouse_start_right_z = y;
        isRightClick = 1;
    } else if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
        zoom += change_right_z;
        change_right_z = 0;
        isRightClick = 0;
    } 
}

void keyDown(unsigned char key, int x, int y) {
    if (key == 27) {
        exit(0);
    }
    if (key < 97 || key > 122) {
        return;
    }
    key = key - 97;
    switch (key) {
        case FORWARD:
            keyboard[key] = 1;
            break;
        case BACKWARD:
            keyboard[key] = 1;
            break;
        case LEFT:
            keyboard[key] = 1;
            break;
        case RIGHT:
            keyboard[key] = 1;
            break;
        case QUIT:
            exit(0);
    }
}

void keyUp(unsigned char key, int x, int y) {
    if (key < 97 || key > 122) {
        return;
    }
    key = key - 97;
    keyboard[key] = !keyboard[key];
}

void reshape(int width, int height) {

    glViewport(0,0,width, height);
    glMatrixMode(GL_PROJECTION);    
    glLoadIdentity();              
    gluPerspective(75, (float)width/(float)height, 0.01, 100.0); 
    glTranslatef(0, 0, -1);
    glMatrixMode(GL_MODELVIEW);  

    printf("w: %d h:%d", width, height);
}


int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutCreateWindow("Assignment 1");

    /* In this program these OpenGL calls only need to be done once,
       so they are in main rather than display. */
    glOrtho(-1, 1, -1, 1, -2, 2);


    init();
    glEnable(GL_DEPTH_TEST);

    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutMotionFunc(mouseMove);
    glutMouseFunc(mouseStart);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyDown);
    glutKeyboardUpFunc(keyUp);

    glutMainLoop();

    return EXIT_SUCCESS;
}
