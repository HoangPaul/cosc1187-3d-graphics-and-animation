#include "Helper.h"

void normalise(float x, float y, float z, float *nx, float *ny, float *nz) {
    float dist;

    dist = sqrt( x*x + y*y + z*z);
    *nx = x/dist;
    *ny = y/dist;
    *nz = z/dist;
}

float angle_between_vec(float x1, float y1, float z1, float x2, float y2, float z2) {
    float dot_product = x1 * x2 + y1 * y2 + z1 * z2;
    float dist_vec1 = sqrt(x1*x1 + y1*y1 + z1*z1);
    float dist_vec2 = sqrt(x2*x2 + y2*y2 + z2*z2);
    return acos(dot_product/(dist_vec1 * dist_vec2));
}
