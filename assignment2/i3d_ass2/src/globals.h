#pragma once

#if __cplusplus
extern "C" {
#endif

#include "camera.h"
#include "mesh.h"
#include "controls.h"
#include "player.h"
#include "grid.h"
#include "light.h"


    typedef struct Globals Globals;

    ///
    /// Everything that is stored by the game can be found here. Just makes things easier.
    ///
    struct Globals
    {


        Camera camera0;
        Camera camera1;
        Camera textCamera;
        Camera skybox;

        PlayerControls playerControls;
        PlayerControls playerControls1;
        CameraControls cameraControls;
        DebugControls debugControls;

        Player player;
        Player player1;
        Grid grid;
        Grid grid1;

        Light light;

        Mesh *testCube;

        Trees trees;
        Particles particles;
        Particles particles1;
 
        float timer;
    };

    ///
    /// Initialises variables in globals with default values, and puts OpenGL into a default state for rendering. Expects globals to already have been constructed.
    ///
    void initGlobals(Globals *globals);

#if __cplusplus
}
#endif
