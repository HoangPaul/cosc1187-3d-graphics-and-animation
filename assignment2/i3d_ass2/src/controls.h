#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

typedef struct PlayerControls PlayerControls;
typedef struct CameraControls CameraControls;
typedef struct DebugControls DebugControls;

///
/// PlayerControls struct is used for player movement. It stores information for wether keys are pressed and nothing else.
///
struct PlayerControls
{
	bool up;
	bool down;
	bool left;
	bool right;
};

///
/// Sets values of player controls to false. Expects player controls to already have been constructed.
///
void initPlayerControls(PlayerControls *controls);


///
/// CameraControls used for camera rotation/zoom, stores information for wether left/right mouse button is pressed.
///
struct CameraControls
{
	bool rotating;
	bool zooming;
};

///
/// Sets values of camera controls to false. Expects camera controls to already have been constructed.
///
void initCameraControls(CameraControls *controls);


///
/// DebugControls used for rendering of debug information, such as toggling wifreframe/lighting, rendering lines for normals, axes, etc.
///
struct DebugControls
{
	bool lightingFlag;
	bool wireframeFlag;
	bool normalFlag;
	bool tangentFlag;
	bool binormalFlag;
	bool axisFlag;
};

///
/// Sets values of debug controls to false (except lighting, which is true). Expects debug controls to already have been constructed.
///
void initDebugControls(DebugControls *controls);

///
/// Sets the value of glPolygonMode to either GL_LINE or GL_FILL based on the wireframe flag in debug controls.
///
void toggleWireframe(DebugControls *controls);

///
/// Enables or disables lighting based on the lighting flag in debug controls.
///
void toggleLighting(DebugControls *controls);


#if __cplusplus
}
#endif
