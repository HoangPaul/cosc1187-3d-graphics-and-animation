#include "player.h"
#include "controls.h"
#include "grid.h"
#include "mesh.h"
#include "gl.h"

#include <stdio.h>
#include <math.h>

#define UNUSED(x) (void)(x)
#define GRAVITY_ACCELERATION -9.8 * 0.1

static void updatePlayerPos(Player *player, PlayerControls *controls, Grid *grid, float dt, Trees *trees)
{
    // dir determines wether the player is moving forwards or backwards (or not at all).
    float dir = 0;
    if (controls->up)
        dir += 1;
    if (controls->down)
        dir -= 1;

    // turn determines the rotation of the player around its y axis.
    float turn = 0;
    if (controls->left)
        turn += player->turnSpeed;
    if (controls->right)
        turn -= player->turnSpeed;
    player->rot.y += turn * dt;

    // y position of the player.
    float gridHeight = accumHeight(grid, player->pos.x, player->pos.z) + 1;
    
    /* oh god you're looking at this code */

    //tree collision
    bool hasCollided = false;
    for (int i = 0; i < trees->numTrees; i++) {
        float posX = player->pos.x - trees->pos[i].x;
        float posZ = player->pos.z - trees->pos[i].z;
        float dist = sqrt(posX * posX + posZ * posZ);
        if (player->radius + trees->radius > dist) {
            hasCollided = true;
            if (!player->isCollide) {
                player->isCollide = true;
                player->numTreeCollision++;
            }
        }
    }
    
    if (!hasCollided) {
        player->isCollide = false;
    }

     //air time
    if (player->pos.y - gridHeight > 0.5f) {
        player->airtime += dt;
        player->isAirbourne = true;
    } else {
        player->isAirbourne = false;
    }
    
    if (player->pos.y <= gridHeight ) {
        player->vel.y -= GRAVITY_ACCELERATION;
    }

    // velocity is calculated using the x and z values of the player's forward vector (which is calculated using the player's y rotation).
    player->vel.y += GRAVITY_ACCELERATION;
    if (player->pos.y < gridHeight) {
        player->vel.y = 0;
        player->pos.y = gridHeight;
    }
    if (!player->isAirbourne) {
        player->vel.x += player->forward.x * player->acc * dir;
        player->vel.z += player->forward.z * player->acc * dir;
    }

    //sliding down slope
    if (!player->isAirbourne) {
        player->vel.y += GRAVITY_ACCELERATION * dt * cos(player->rot.x * (float) M_PI / 180.0f);
        player->vel.x += GRAVITY_ACCELERATION * dt * sin(player->rot.z * (float) M_PI / 180.0f);
        player->vel.z += -GRAVITY_ACCELERATION * dt * sin(player->rot.x * (float) M_PI / 180.0f);
    }
    //friction
    if (!player->isAirbourne) {
        player->vel.x *= pow(1 - 0.1f, dt);
        player->vel.z *= pow(1 - 0.1f, dt);
    }

    //drag
    float drag_coefficient = 0.01;
    float dragY = 0.5f * drag_coefficient * player->vel.y * player->vel.y;
    float dragX = 0.5f * drag_coefficient * player->vel.x * player->vel.x;
    float dragZ = 0.5f * drag_coefficient * player->vel.z * player->vel.z;
    player->vel.x += player->forward.x * dragX* -dir;
    player->vel.z += player->forward.z * dragZ* -dir;
    player->vel.y += dragY;     //??

    //jumps
    if (player->pos.y - gridHeight < 0.01) {
        player->vel.y += player->forward.y * player->speed;
    }

    //temp hard limits
    if (player->vel.x > 50 || player->vel.x < -50 ) {
        player->vel.x = 0;
    }
    if (player->vel.y > 50 || player->vel.y < -50 ) {
        player->vel.y = 0;
    }
    if (player->vel.z > 50 || player->vel.z < -50 ) {
        player->vel.z = 0;
    }
    

    // position is simple to calculate when given correct velocity (note that velocity does not need to be the same as the forward direction of the player).
    //player->pos.y += player->vel.y * dt;
    //player->pos.x += player->vel.x * dt;
    //player->pos.z += player->vel.z * dt;
    player->pos.y += dt * (player->vel.y + dt * 0.5);
    player->pos.x += dt * (player->vel.x + dt * 0.5);
    player->pos.z += dt * (player->vel.z + dt * 0.5);
    player->speed = sqrt(player->vel.x * player->vel.x + player->vel.y * player->vel.y + player->vel.z * player->vel.z);
}

static void orientPlayer(Player *player, Grid *grid, float dt)
{
    UNUSED(dt);

    // only x and z values of the player's forward vector are needed right now, but y is provided as well for completeness (it'll be useful later).
    player->forward.x = sinf(player->rot.y * (float) M_PI / 180.0f);
    player->forward.z = cosf(player->rot.y * (float) M_PI / 180.0f);
    player->forward.y = -sinf(player->rot.x * (float) M_PI / 180.0f);

    vec3f v = accumNormal(grid, player->pos.x, player->pos.z);

    // the player's rotation around the x (pitch) axis is calculated with a dot product of the normal vector, with the forward vector (y value is not needed).
    player->rot.x = dotVec3f(cVec3f(player->forward.x, 0, player->forward.z), v) * 180.0f / (float) M_PI;

    // the player's right vector can be calculated in the same way as the forward vector, and a dot product used to give the rotation on z (roll).
    float rx = sinf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
    float rz = cosf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
    player->rot.z = dotVec3f(cVec3f(rx, 0, rz), v) * 180.0f / (float) M_PI;

    // if you want to be tricky, you don't actually need any trig for the z rotation, ie:
    // float rx = sinf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
    // float rz = cosf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
    //
    // is the same as:
    // float rx = -cosf(player->rot.y * (float) M_PI / 180.0f);
    // float rz = sinf(player->rot.y * (float) M_PI / 180.0f);
    //
    // which is the same as:
    // float rx = -player->forward.z;
    // float rz = player->forward.x;
    //
    // therefore the final calculation for z (roll) is:
    // player->rot.z = dotVec3f(cVec3f(-player->forward.z, 0, player->forward.x), v) * 180.0f / (float) M_PI;
}




void initPlayer(Player *player, vec3f pos, vec3f rot, float speed, float turnSpeed, float radius, char playername[])
{
    player->forward = cVec3f(0, 0, 1);
    player->pos = pos;
    player->rot = rot;
    player->speed = speed;
    player->turnSpeed = turnSpeed;
    player->radius = radius;
    player->acc = 1.0f;
    player->maxSpeed = 15;
    sprintf(player->playername, playername);
    player->airtime = 0;

    player->pos.y = 50;//accumHeight(grid, player->pos.x, player->pos.z) + 1;

}

void initParticles(Particles *particles) {
    particles->numParticles = 200;
    particles->pos = calloc(particles->numParticles, sizeof(vec3f));
    particles->dir = calloc(particles->numParticles, sizeof(vec3f));
    particles->timeAlive = calloc(particles->numParticles, sizeof(float));

    for (int i = 0; i < particles->numParticles; i++) {
        particles->pos[i].x = -1;
        particles->pos[i].y = -1;
        particles->pos[i].z = -1;

        particles->dir[i].x = rand()%6 - 3;
        particles->dir[i].y = rand()%6 - 3;
        particles->dir[i].z = -rand()%8 - 2;
        particles->timeAlive[i] = 5.0*((float)i)/particles->numParticles;
    }
}

bool isPlayerNearOutOfBounds(Player *player, Grid *grid) {
    return  player->pos.x > grid->pos.x + grid->size.x/2 ||
            player->pos.x < grid->pos.x - grid->size.x/2 ||
            player->pos.z > grid->pos.y + grid->size.y/2 ||
            player->pos.z < grid->pos.y - grid->size.y/2 ;
}

void updatePlayer(Player *player, float dt, Grid *grid, PlayerControls *controls, Trees *trees)
{
    updatePlayerPos(player, controls, grid, dt, trees);
    orientPlayer(player, grid, dt);
    if (isPlayerNearOutOfBounds(player, grid)) {
        calcMeshVerts(grid, cVec2f(player->pos.x, player->pos.z));
    }
}



void renderPlayer(Player *player, Mesh *mesh, DebugControls *controls)
{
    static float diffuse[] = { 1, 0.5f, 0, 1 };
    static float ambient[] = { 0.4f, 0.2f, 0, 1 };
    static float specular[] = { 1, 0.5f, 0, 1 };
    static float shininess = 32.0f;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    glPushMatrix();

    glTranslatef(player->pos.x, player->pos.y, player->pos.z);
    glRotatef(player->rot.y, 0, 1, 0);
    glRotatef(player->rot.x, 1, 0, 0);
    glRotatef(player->rot.z, 0, 0, 1);

    //torso
    glTranslatef(0, 0.2, 0);
    glPushMatrix();
    glScalef(0.3, 0.5, 0.15);
    renderMesh(mesh, controls);
    glPopMatrix();
    
    //head
    glPushMatrix();
        glTranslatef(0, 1, 0);
        glScalef(0.2, 0.2, 0.2);
        renderMesh(mesh, controls);
    glPopMatrix();

    //left arm
    glPushMatrix();
        glScalef(0.5, 0.5, 0.5);
        glTranslatef(1, 0, 0);
        glPushMatrix();
            glTranslatef(0, 0, 0.5);
            glRotatef(30, 1, 0, 0);
            
            glScalef(0.2, 0.2, 0.5);
            renderMesh(mesh, controls);
        glPopMatrix();
        
        glPushMatrix();
            glTranslatef(0, 0.5, 1.5);
            glRotatef(45, -1, 0, 0);
            
            //hand
            glPushMatrix();
                glScalef(0.2, 0.2, 0.2);
                renderMesh(mesh, controls);
            glPopMatrix();
            //stick
            glTranslatef(0, -0.9, -1);
            glRotatef(45, 1, 0, 0);
            glPushMatrix();
                glScalef(0.1, 1, 0.1);
                renderMesh(mesh, controls);
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();

    //right arm 
    glPushMatrix();
        glScalef(0.5, 0.5, 0.5);
        glTranslatef(-1, 0, 0);
        glPushMatrix();
            glTranslatef(0, 0, 0.5);
            glRotatef(30, 1, 0, 0);
            
            glScalef(0.2, 0.2, 0.5);
            renderMesh(mesh, controls);
        glPopMatrix();
        
        glPushMatrix();
            glTranslatef(0, 0.5, 1.5);
            glRotatef(45, -1, 0, 0);
            
            //hand
            glPushMatrix();
                glScalef(0.2, 0.2, 0.2);
                renderMesh(mesh, controls);
            glPopMatrix();
            //stick
            glTranslatef(0, -0.9, -1);
            glRotatef(45, 1, 0, 0);
            glPushMatrix();
                glScalef(0.1, 1, 0.1);
                renderMesh(mesh, controls);
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();

    //legs
    glPushMatrix();
        glTranslatef(0, -0.7, 0.1);
        glRotatef(-20, 1, 0, 0);
        glPushMatrix();
            glScalef(0.25, 0.4, 0.1);
            renderMesh(mesh, controls);
        glPopMatrix();
        glTranslatef(0, -0.3, 0);
        //glRotatef(45, 1, 0, 0);
        glPushMatrix();
            glScalef(0.25, 0.05, 0.5);
            renderMesh(mesh, controls);
        glPopMatrix();
    glPopMatrix();

    glPopMatrix();
}
