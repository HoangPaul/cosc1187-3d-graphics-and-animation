#include "globals.h"
#include "gl.h"

static void initRendering()
{
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);

    glShadeModel(GL_SMOOTH);

    glClearColor(0, 0, 0, 0);
}

void initGlobals(Globals *globals)
{
    // glew is for OpenGL2+ functions, in case you want to use them.
    glewInit();

    initPlayerControls(&globals->playerControls);
    initPlayerControls(&globals->playerControls1);

    initCameraControls(&globals->cameraControls);
    initDebugControls(&globals->debugControls);

    initGrid(&globals->grid, cVec2f(200, 800), 200, 800, 0.25f);
    initGrid(&globals->grid1, cVec2f(200, 800), 200, 800, 0.25f);

    initTrees(&globals->grid, &globals->trees);

    initPlayer(&globals->player, cVec3f(0, 0, 0), cVec3f(0, 0, 0), 15, 45, 1, "Player 1");
    initPlayer(&globals->player1, cVec3f(0, 0, 0), cVec3f(0, 0, 0), 15, 45, 1, "Player 2");

    initLight(&globals->light, cVec4f(100, 100, -100, 0), cVec4f(0.2f, 0.2f, 0.2f, 1), cVec4f(1, 1, 1, 1), cVec4f(0.1f, 0.1f, 0.1f, 1), 64);

    
    initParticles(&globals->particles);
    initParticles(&globals->particles1);

    globals->testCube = makeCube();

    initRendering();

    attachCameraToPlayer(&globals->camera0, &globals->player);
    attachCameraToPlayer(&globals->camera1, &globals->player1);

    globals->timer = 30.0f;

}
