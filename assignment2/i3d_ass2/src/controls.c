#include "controls.h"
#include "gl.h"

void initPlayerControls(PlayerControls *controls)
{
	controls->up = false;
	controls->down = false;
	controls->left = false;
	controls->right = false;
}

void initCameraControls(CameraControls *controls)
{
	controls->rotating = false;
	controls->zooming = false;
}

void initDebugControls(DebugControls *controls)
{
	controls->lightingFlag = true;
	controls->wireframeFlag = false;
	controls->normalFlag = false;
	controls->tangentFlag = false;
	controls->binormalFlag = false;
	controls->axisFlag = false;
}

void toggleWireframe(DebugControls *controls)
{
	if (controls->wireframeFlag)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void toggleLighting(DebugControls *controls)
{
	if (controls->lightingFlag)
		glEnable(GL_LIGHTING);
	else
		glDisable(GL_LIGHTING);
}
