#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

    typedef struct Player Player;
    typedef struct PlayerControls PlayerControls;
    typedef struct Grid Grid;
    typedef struct DebugControls DebugControls;
    typedef struct Mesh Mesh;
    typedef struct Trees Trees;
    typedef struct Particles Particles;
    ///
    /// Contains all data needed to position and move/rotate an object in a world (in this case the player).
    ///
    struct Player
    {
        vec3f pos;
        vec3f rot;
        vec3f vel;
        vec3f forward;

        float speed;
        float turnSpeed;
    
        float maxSpeed;
        float acc;

        float radius;

        char playername[10];
        float airtime;
        int numTreeCollision;
        bool isCollide;
        bool isAirbourne;
    };

    struct Particles {
        vec3f *pos;
        vec3f *dir;
        int numParticles;
        float *timeAlive;
        float dt;
    };

    ///
    /// Intialises variables in the player structs using the given arguments. Expects player to have already been constructed.
    ///
    void initPlayer(Player *player, vec3f pos, vec3f rot, float speed, float turnSpeed, float radius, char playername[]);

    void initParticles(Particles *particles);

    ///
    /// Updates the position and orientation of the player.
    ///
    void updatePlayer(Player *player, float dt, Grid *grid, PlayerControls *controls, Trees *trees);

    ///
    /// Renders the given mesh, using data in the player to position it correctly in the world.
    ///
    void renderPlayer(Player *player, Mesh *mesh, DebugControls *controls);
#if __cplusplus
}
#endif
