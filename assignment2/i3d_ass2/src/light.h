#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

typedef struct Light Light;

///
/// Pretty self explanatory light struct, just here to make things easier.
///
struct Light
{
	vec4f pos;
	vec4f ambient;
	vec4f diffuse;
	vec4f specular;
	float shininess;
};

///
/// Initialises variables in the light with the given arguments. Expects light to already have been constructed.
///
void initLight(Light *light, vec4f pos, vec4f ambient, vec4f diffuse, vec4f specular, float shininess);

///
/// Calls glLightf using all the variables stored in the light. Doesn't render anything, use it to upload light data before rendering something that needs to be lit.
///
void renderLight(Light *light);

#if __cplusplus
}
#endif
