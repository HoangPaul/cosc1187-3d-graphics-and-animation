#include "gl.h"
#include "globals.h"
#include <stdio.h>
#include <math.h>
#include <string.h>

#define UNUSED(x) (void)(x)

Globals globals;
static GLuint textureTree;
static GLuint textureParticles;
static GLuint textureMesh;

static GLuint negx;
static GLuint negy;
static GLuint negz;
static GLuint posx;
static GLuint posy;
static GLuint posz;

bool hasEnded = false;
bool player0win = false;
bool player1win = false;
bool draw = false;

static float getDeltaTime()
{
    // Returns time since previous frame in seconds.
    static int t1 = -1;

    if (t1 == -1)
        t1 = glutGet(GLUT_ELAPSED_TIME);
    int t2 = glutGet(GLUT_ELAPSED_TIME);
    float dt = (t2 - t1) / 1000.0f;
    t1 = t2;
    return dt;
}

static void updateKeyChar(unsigned char key, bool state)
{
    // Sets flags inside player controls, currently using w/a/s/d.
    switch (key)
    {
    case 'w':
        globals.playerControls.up = state;
        break;

    case 's':
        globals.playerControls.down = state;
        break;

    case 'a':
        globals.playerControls.left = state;
        break;

    case 'd':
        globals.playerControls.right = state;
        break;
    case 'u':
        globals.playerControls1.up = state;
        break;

    case 'j':
        globals.playerControls1.down = state;
        break;

    case 'h':
        globals.playerControls1.left = state;
        break;

    case 'k':
        globals.playerControls1.right = state;
        break;

    default:
        break;
    }
}

void renderTrees(Camera *camera, Trees *trees) {
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureTree);

    glEnable(GL_DEPTH_TEST);

    for (int i = 0; i < trees->numTrees; i++) {
        glPushMatrix();
        glTranslatef(trees->pos[i].x, trees->pos[i].y, trees->pos[i].z);
        glRotatef(-camera->rot.y, 0, 1, 0);
        glBegin(GL_QUADS);
        glTexCoord2f(1, 0);
        glVertex3f(1, -1,0);

        glTexCoord2f(1, 1);
        glVertex3f(1, 1,0);        

        glTexCoord2f(0, 1);
        glVertex3f(-1, 1,0);
        

        glTexCoord2f(0, 0);
        glVertex3f(-1, -1, 0);
        
        glEnd();
        glPopMatrix();
    }


    glDisable(GL_TEXTURE_2D);
    glDisable(GL_ALPHA_TEST); 

}

void updateParticles(Player *player, Particles *particles) {
 //particle positions
    for (int i = 0; i < particles->numParticles; i++) {
        particles->timeAlive[i] += particles->dt;
        if (particles->timeAlive[i]  > 5) {
            particles->timeAlive[i]  = 0;
            if (!player->isAirbourne) {
                particles->pos[i].x = player->pos.x  + ((float)(rand()%3 - 1))/2.0;
                particles->pos[i].y = player->pos.y - 0.9;
                particles->pos[i].z = player->pos.z  + ((float)(rand()%3 - 1))/2.0;

                particles->dir[i].x = (player->forward.x) + (rand()%12 - 6);
                particles->dir[i].y = (player->forward.y) + (rand()%3 + 10);
                particles->dir[i].z = (player->forward.z) + (rand()%10);
            }

        }
    }
    for (int i = 0; i < particles->numParticles; i++) {
        particles->dir[i].y += -9.8 * particles->dt;
        particles->pos[i].x += particles->dir[i].x * 0.1 * particles->dt;
        particles->pos[i].y += particles->dir[i].y * 0.01;
        particles->pos[i].z += particles->dir[i].z * 0.1 * particles->dt;
    }
}

void renderParticles(Particles *particles) {
    //render particles
    glEnable(GL_POINT_SPRITE);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPointSize(10);

    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.1);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureParticles);
    glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  
    //draw arrays method
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, particles->pos);

    glDrawArrays(GL_POINTS, 0, particles->numParticles);
    glDisableClientState(GL_VERTEX_ARRAY);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);

/*  immediate mode
    glBegin(GL_POINTS);
    for (int i = 0; i < particles->numParticles; i++) {
        glVertex3f(particles->pos[i].x, particles->pos[i].y, particles->pos[i].z);
    }
    glEnd();
*/

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST); 
    glDisable(GL_POINT_SPRITE);
    glDisable(GL_POINT_SMOOTH);
}

void renderTextPlayer(Camera *camera, Player *player) {
    glLoadIdentity(); 
    glViewport(camera->left/2,0, camera->right, camera->height);
    glMatrixMode(GL_PROJECTION);   
    glLoadIdentity(); 
    glOrtho(-1, 1, -1, 1, -1, 1);
    glMatrixMode(GL_MODELVIEW); 

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);   
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glColor3f(0, 0, 1.0f);

    int i;

    //Player name
    glRasterPos2f(-0.1 , 0.9);
    for (i = 0; i < 8; i++) {
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, player->playername[i]);
    }

    //air time
    glRasterPos2f(-0.1 , 0.85);
    char airtimeText[20];
    memset(airtimeText,'\0', 20);
    sprintf(airtimeText, "Air time: %.2f", player->airtime);
    for (i = 0; i < 20; i++) {
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, airtimeText[i]);
    }

    //collisions
    glRasterPos2f(-0.1 , 0.80);
    char collisionsText[20];
    memset(collisionsText,'\0', 20);
    sprintf(collisionsText, "Collisions: %d", player->numTreeCollision);
    for (i = 0; i < 20; i++) {
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, collisionsText[i]);
    }


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}

void renderTextTimer(Globals *globals, Camera *camera) {
    glLoadIdentity(); 
    glViewport(0,0, camera->right, camera->height);
    glMatrixMode(GL_PROJECTION);   
    glLoadIdentity(); 
    glOrtho(-1, 1, -1, 1, -1, 1);
    glMatrixMode(GL_MODELVIEW); 

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glColor3f(0, 0, 1.0f);

    char time[20];
    int i;
    glRasterPos2f(-0.1 , 0.9);
    memset(time, '\0', 20);
    sprintf(time, "Time: %.2f", globals->timer);

    for (i = 0; i < 10; i++) {
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, time[i]);
    }

    char msg[20];
    memset(msg, '\0', 20);
    glRasterPos2f(-0.1 , 0.5);
    if (hasEnded) {
        if (player0win) {
            sprintf(msg, "Player 1 wins!");
        } else if (player1win) {
            sprintf(msg, "Player 2 wins!");
        } else if (draw) {
            sprintf(msg, "Draw!");
        } else {
            sprintf(msg, "I don't");
        }
        for (i = 0; i < 20; i++) {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msg[i]);
        }
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
}

void renderSkybox(Camera *camera) {
    
    glPushMatrix();
    glLoadIdentity();
    uploadProjection(camera);
    glTranslatef(0,0,-0.5);
    glRotatef(camera->rot.x, 1, 0, 0);
    glRotatef(camera->rot.y, 0, 1, 0);

    glColor3f(1,1,1);
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);


    // Render the front quad
    glBindTexture(GL_TEXTURE_2D, negx);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1); glVertex3f(  0.5f,  0.5f, -0.5f );
    glTexCoord2f(1, 1); glVertex3f( -0.5f,  0.5f, -0.5f );
    glTexCoord2f(1, 0); glVertex3f( -0.5f, -0.5f, -0.5f );
    glTexCoord2f(0, 0); glVertex3f(  0.5f, -0.5f, -0.5f );
    glEnd();
    // Render the left quad
    glBindTexture(GL_TEXTURE_2D, posz);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1); glVertex3f(  0.5f,  0.5f,  0.5f );
    glTexCoord2f(1, 1); glVertex3f(  0.5f,  0.5f, -0.5f );
    glTexCoord2f(1, 0); glVertex3f(  0.5f, -0.5f, -0.5f );
    glTexCoord2f(0, 0); glVertex3f(  0.5f, -0.5f,  0.5f );
    glEnd();
    // Render the back quad
    glBindTexture(GL_TEXTURE_2D, posx);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1); glVertex3f( -0.5f,  0.5f,  0.5f );
    glTexCoord2f(1, 1); glVertex3f(  0.5f,  0.5f,  0.5f );
    glTexCoord2f(1, 0); glVertex3f(  0.5f, -0.5f,  0.5f );
    glTexCoord2f(0, 0); glVertex3f( -0.5f, -0.5f,  0.5f );
    glEnd();
    // Render the right quad
    glBindTexture(GL_TEXTURE_2D, negz);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1); glVertex3f( -0.5f,  0.5f, -0.5f );
    glTexCoord2f(1, 1); glVertex3f( -0.5f,  0.5f,  0.5f );
    glTexCoord2f(1, 0); glVertex3f( -0.5f, -0.5f,  0.5f );
    glTexCoord2f(0, 0); glVertex3f( -0.5f, -0.5f, -0.5f );
    glEnd();
    // Render the top quad
    glBindTexture(GL_TEXTURE_2D, posy);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBegin(GL_QUADS);
    glTexCoord2f(1, 1); glVertex3f(  0.5f,  0.5f, -0.5f );
    glTexCoord2f(1, 0); glVertex3f(  0.5f,  0.5f,  0.5f );
    glTexCoord2f(0, 0); glVertex3f( -0.5f,  0.5f,  0.5f );
     glTexCoord2f(0, 1); glVertex3f( -0.5f,  0.5f, -0.5f );
    glEnd();
    // Render the bottom quad
    glBindTexture(GL_TEXTURE_2D, negy);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3f( -0.5f, -0.5f, -0.5f );
    glTexCoord2f(0, 1); glVertex3f( -0.5f, -0.5f,  0.5f );
    glTexCoord2f(1, 1); glVertex3f(  0.5f, -0.5f,  0.5f );
    glTexCoord2f(1, 0); glVertex3f(  0.5f, -0.5f, -0.5f );
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    
    glPopMatrix();
}

static void renderViewport(Globals *globals, Grid *grid, Camera *camera)
{
    // Renders a viewport using the given camera. Useful to have this in its own function in case you want to render multiple viewports using different cameras.
    uploadProjection(camera);
    uploadView(camera);

    renderLight(&globals->light);

    //texturing for grid
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureMesh);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    renderGrid(grid, &globals->debugControls);
    glDisable(GL_TEXTURE_2D);
}

static void renderPlayerWithViewport(Globals *globals, Grid *grid, Camera *camera) {
    renderSkybox(camera);
    renderViewport(globals, grid, camera);
    renderTrees(camera, &globals->trees);
    renderParticles(&globals->particles);
    renderParticles(&globals->particles1);
    renderPlayer(&globals->player, globals->testCube, &globals->debugControls);    
    renderPlayer(&globals->player1, globals->testCube, &globals->debugControls);
}

static void render(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderPlayerWithViewport(&globals, &globals.grid, &globals.camera0);
    
    renderPlayerWithViewport(&globals, &globals.grid1, &globals.camera1);

    renderTextPlayer(&globals.camera0, &globals.player);
    renderTextPlayer(&globals.camera1, &globals.player1);
    renderTextTimer(&globals, &globals.textCamera);


    glutSwapBuffers();
}

static void update(void)
{
    if (globals.player.numTreeCollision >= 5) {
        hasEnded = true;
        player1win = true;
    }
    if (globals.player1.numTreeCollision >= 5) {
        hasEnded = true;
        player0win = true;
    }
    if (globals.timer <= 0) {
        hasEnded = true;
        if (globals.player.airtime > globals.player1.airtime) {
            player0win = true;
        } else if (globals.player.airtime < globals.player1.airtime) {
            player1win = true;
        } else {
            draw = true;
        }
    }
    if (!hasEnded) {
        float dt = getDeltaTime();
        globals.particles.dt = dt;
        globals.particles1.dt = dt;
        if (dt > 0) {
            globals.timer -= dt;
        }

        updatePlayer(&globals.player, dt, &globals.grid, &globals.playerControls, &globals.trees);
        updatePlayer(&globals.player1, dt, &globals.grid1, &globals.playerControls1, &globals.trees);
        updateParticles(&globals.player, &globals.particles);
        updateParticles(&globals.player1, &globals.particles1);
    }
 
    glutPostRedisplay();
}

static void reshape(int width, int height)
{
    globals.camera0.right = width/2;
    globals.camera1.left = globals.camera0.right;
    globals.camera1.right = width;
    globals.camera0.height = height;
    globals.camera1.height = height;
    globals.textCamera.left = 0;
    globals.textCamera.right = width;
    globals.textCamera.height = height;
    globals.skybox.left = 0;
    globals.skybox.right = width;
    globals.skybox.height = height;

    // So that the program will continue rendering as the window is resized.
    render();
}

static void mouseMove(int x, int y)
{
    static int prevX, prevY;

    int dx = x - prevX;
    int dy = y - prevY;

    if (globals.cameraControls.rotating) {
        updateRotation(&globals.camera0, dy, dx);
        updateRotation(&globals.camera1, dy, dx);
    }

    if (globals.cameraControls.zooming) {
        updateZoom(&globals.camera0, dy);
        updateZoom(&globals.camera1, dy);
    }

    prevX = x;
    prevY = y;
}

static void mouseDown(int button, int state, int x, int y)
{
    UNUSED(x);
    UNUSED(y);

    // Sets flags inside camera controls, currently just uses the left and right mouse buttons.
    if (button == GLUT_LEFT_BUTTON)
        globals.cameraControls.rotating = state == GLUT_DOWN;
    if (button == GLUT_RIGHT_BUTTON)
        globals.cameraControls.zooming = state == GLUT_DOWN;
}

static void keyboard(unsigned char key, int x, int y)
{
    UNUSED(x);
    UNUSED(y);

    // Toggles flags in debug controls (as well as quitting using escape).
    switch (key)
    {
    case 27:
    case 'q':
        exit(EXIT_SUCCESS);
        break;

    case 'p':
        globals.debugControls.wireframeFlag = !globals.debugControls.wireframeFlag;
        toggleWireframe(&globals.debugControls);
        break;

    case 'l':
        globals.debugControls.lightingFlag = !globals.debugControls.lightingFlag;
        toggleLighting(&globals.debugControls);
        break;

    case 'n':
        globals.debugControls.normalFlag = !globals.debugControls.normalFlag;
        break;

    case 't':
        globals.debugControls.tangentFlag = !globals.debugControls.tangentFlag;
        break;

    case 'b':
        globals.debugControls.binormalFlag = !globals.debugControls.binormalFlag;
        break;

    case 'o':
        globals.debugControls.axisFlag = !globals.debugControls.axisFlag;
        break;

        // If any other key is pressed, check if it's used for player movement.
    default:
        updateKeyChar(key, true);
        break;
    }
}

static void keyboardUp(unsigned char key, int x, int y)
{
    UNUSED(x);
    UNUSED(y);

    // Key has been released, check if it's used for player movement.
    updateKeyChar(key, false);
}



int main(int argc, char **argv)
{
    // Window is created using data from the camera, so create that first.
    initCamera(&globals.camera0, 0, 320);
    initCamera(&globals.camera1, 320, 640);
    initCamera(&globals.textCamera, 0, 640);
    initCamera(&globals.skybox, 0, 640);

    glutInit(&argc, argv);
    glutInitWindowSize(globals.camera0.right*2, globals.camera0.height);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutCreateWindow("I3D Assignment");

    // Various callback functions being used.
    glutDisplayFunc(render);
    glutIdleFunc(update);
    glutReshapeFunc(reshape);
    glutMotionFunc(mouseMove);
    glutPassiveMotionFunc(mouseMove);
    glutMouseFunc(mouseDown);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardUp);


    // Globals need to be initialised before the program starts running.
    initGlobals(&globals);
    glEnable(GL_NORMALIZE);

    textureTree = loadTexture("img/tree.png");
    if (!textureTree)
    {
        printf("No texture created; exiting.\n");
        return EXIT_FAILURE;
    }
    textureParticles =loadTexture("img/snowball.png");
    if (!textureParticles)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    textureMesh=loadTexture("img/snow.jpg");
    if (!textureMesh)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    negx=loadTexture("img/negx.bmp");
    if (!negx)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    negy=loadTexture("img/negy.bmp");
    if (!negy)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    negz=loadTexture("img/negz.bmp");
    if (!negz)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    posx=loadTexture("img/posx.bmp");
    if (!posx)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    posy=loadTexture("img/posy.bmp");
    if (!posy)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }
    posz=loadTexture("img/posz.bmp");
    if (!posz)
    {
        printf("No texture created; exiting.\n");
        exit(EXIT_FAILURE);
    }


    glutMainLoop();

    return EXIT_SUCCESS;
}

