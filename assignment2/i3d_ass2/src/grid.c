#include "grid.h"
#include "gl.h"
#include "mesh.h"

#include <math.h>

// Just some default sine functions, feel free to play around with these.
static SineFunction sineFunctions[] = {
    // { 0.5f, 0, 0, 1 },
    // { 0.5f, 0.5, 1, 0 },
    { 0.2f, 0.0f, 0.17f, 0.47f },
    { 0.4f, 0.0f, 0.15f, 0.53f },
    { 8, 0.0f, 0.05f, 0.004f },
};

void calcMeshVerts(Grid *grid, vec2f pos)
{
    // Populates mesh with appropriate vertex data, using the sine functions given above.
    float xInc = grid->size.x / (float) (grid->rows - 2);
    float yInc = grid->size.y / (float) (grid->cols - 2);
    grid->pos = pos;

    size_t l = 0;
    float x = -grid->size.x / 2.0f + pos.x;
    for (size_t i = 0; i < grid->rows; i++)
    {
        float y = -grid->size.y / 2.0f + pos.y;
        //float y = -10;
        for (size_t j = 0; j < grid->cols; j++)
        {
            grid->mesh->vertices[l].t = cVec2f(x / (grid->size.x), y / (grid->size.y));
            grid->mesh->vertices[l].p = cVec3f(x, accumHeight(grid, x, y), y);
            grid->mesh->vertices[l].n = accumNormal(grid, x, y);
            grid->mesh->vertices[l].tan = accumTangent(grid, x, y);
            grid->mesh->vertices[l].bin = accumBinormal(grid, x, y);

            y += yInc;
            l++;
        }

        x += xInc;
    }
}




void initGrid(Grid *grid, vec2f size, size_t rows, size_t cols, float slopeAmt)
{
    grid->size = size;
    grid->rows = rows;
    grid->cols = cols;
    grid->sineFunctions = sineFunctions;
    grid->nSineFunctions = 3;
    grid->slopeAmt = slopeAmt;
    grid->pos = cVec2f(0,0);

    grid->mesh = cMesh(grid->rows * grid->cols, (grid->rows - 1) * (grid->cols - 1) * 6);
    calcMeshVerts(grid, cVec2f(0,0));

    size_t iPos = 0, vPos = 0;
    for (size_t i = 0; i < grid->rows - 1; i++)
    {
        for (size_t j = 0; j < grid->cols - 1; j++)
        {
            grid->mesh->indices[iPos++] = vPos;
            grid->mesh->indices[iPos++] = vPos + 1;
            grid->mesh->indices[iPos++] = vPos + grid->cols;
            grid->mesh->indices[iPos++] = vPos + grid->cols;
            grid->mesh->indices[iPos++] = vPos + 1;
            grid->mesh->indices[iPos++] = vPos + grid->cols + 1;
            vPos++;
        }
        vPos++;
    }


    // You could alternatively use these functions to create tangent/normal vectors rather than calculating them analytically.
    // calcTangents(grid->mesh);
    // calcNormals(grid->mesh, true);
}

void initTrees(Grid *grid, Trees *trees) {
    trees->numTrees = 200;
    trees->radius = 1;
    trees->pos = calloc(trees->numTrees, sizeof(vec3f));

    float randX = 0;
    float randZ = 0;
    float randY = 0;
    for (int j = 0; j < trees->numTrees; j++) {
        randX = (rand()%(int)grid->size.x) - (int)grid->size.x/2;
        randZ = (rand()%(int)grid->size.y) - (int)grid->size.x/2;
        randY = accumHeight(grid, randX, randZ) + 1;
        trees->pos[j].x = randX;
        trees->pos[j].y = randY;
        trees->pos[j].z = randZ;
    }

}

void renderGrid(Grid *grid, DebugControls *controls)
{
    static float diffuse[] = { 0.9f, 1, 1, 1 };
    static float ambient[] = { 0.2f, 0.2f, 0.2f, 1 };
    static float specular[] = { 1, 1, 1, 1 };
    static float shininess = 32.0f;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

    renderMesh(grid->mesh, controls);
}

float accumHeight(Grid *grid, float x, float z)
{
    // Calculates height for each sine function and adds them together to give the final value, below functions follow the same behaviour.
    float height = 0;
    for (size_t i = 0; i < grid->nSineFunctions; i++)
        height += calcHeight(&grid->sineFunctions[i], x, z);

    // This is just here to make the slope appear to move downwards along the z axis.
    height -= z * grid->slopeAmt;

    return height;
}

vec3f accumNormal(Grid *grid, float x, float z)
{
    vec3f v = cVec3f(0, 1, 0);
    for (size_t i = 0; i < grid->nSineFunctions; i++)
    {
        vec3f n = calcNormal(&grid->sineFunctions[i], x, z);
        v.x += n.x;
        v.z += n.z;
    }
    v.z += grid->slopeAmt;
    return normaliseVec3f(v);
}

vec3f accumTangent(Grid *grid, float x, float z)
{
    float y = 0;
    for (size_t i = 0; i < grid->nSineFunctions; i++)
        y += calcTangent(&grid->sineFunctions[i], x, z).y;
    return normaliseVec3f(cVec3f(1, y, 0));
}

vec3f accumBinormal(Grid *grid, float x, float z)
{
    float y = 0;
    for (size_t i = 0; i < grid->nSineFunctions; i++)
        y += calcBinormal(&grid->sineFunctions[i], x, z).y;
    y -= grid->slopeAmt;
    return normaliseVec3f(cVec3f(0, y, 1));
}
